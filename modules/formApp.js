import { lastMidi, soundList, controlList } from './midiInit.js';


export class controler extends FormApplication {
    constructor(data, options = {}) {
        super(options);

        this.midi = lastMidi;
        this.controls = controlList;
        this.soundList = soundList
    }
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["form"],
            isEditable: true,
            submitOnChange: true,
            submitOnClose: true,
            closeOnSubmit: false,
            popOut: true,
            width: 600,
            height: 600,
            left: 150,
            template: "modules/midi control/templates/midiControl.html",
            id: "midi-control-list",
            title: "midi control list"
        });
    };

    getData() {
        const data = super.getData()
            // Return data to the template
        this.midi = lastMidi;
        this.
        return {

        }

    }

    activateListeners(html) {
        super.activateListeners(html);
        console.log('controlist =--------')
        console.log(controlList);
        console.log("objet de la formapp=");
        console.log(this);
        let addControl = html.find('span.addControl')[0];
        addControl.addEventListener('click', () => { this.addControler(lastMidi) });
        let selectEntity = html.find('select.selectEntity');
        for (let sel of selectEntity) {
            sel.addEventListener('change', () => { this.changeEntity() })
        };
    }



    addControler(midi) {
        // console.log(midi);
        console.log(lastMidi);
        if (midi.length > 0) {
            ui.notifications.info("new webmidi control : " + midi);
            var newCont = {};
            newCont.midiTrig = midi;
            newCont.entity = new String;
            newCont.id = this.controls.length + 1;
            this.controls.push(newCont);

            console.log(this.controls);

            this.render(true);
        } else {
            ui.notifications.warn("no webmidi signal received");

        }
    }

    changeEntity() {
        let controls = this.controls;
        console.log(controlList);
        this._updateObject()
    }
    async _updateObject(lastMidi, formData) {

        this.getData();

    }
}
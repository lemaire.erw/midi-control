export var lastMidi;
export var controlList = [];
export var soundList = [];
export var midiEnabled = true;

Hooks.once("ready", async function() {

    //----------------midi----------
    navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);



    function onMIDIFailure() {
        ui.notifications.info("your browser doesn't accept webmidi");
    }

    function onMIDISuccess(midiAccess) {
        midiEnabled = true;
        ui.notifications.info("your browser accept webmidi");
        for (var input of midiAccess.inputs.values())
            input.onmidimessage = getMIDIMessage;

    }


    function getMIDIMessage(midiMessage) {

        //-----------active le suivi de midi 


        lastMidi = midiMessage.data;
        console.log(lastMidi);
        let midiIn = document.getElementById("midiIn");
        if (midiIn) {
            midiIn.value = lastMidi;
        }
        /*
        for (let ct in controlList) {
            if (lastMidi = ct.midiTrig) {
                ui.notifications.info("j'ai un trig")
            }
        }
*/
    }





});

//------------get sounds list
Hooks.on("renderSidebarTab", async function() {

    let soundsClass = document.getElementsByClassName('sound');
    let tempSoundList = [];

    for (let sc of soundsClass) {
        if (sc.getAttribute('data-sound-id')) {
            let sound = {};
            sound.name = sc.firstElementChild.innerHTML;
            sound.id = sc.getAttribute('data-sound-id');
            tempSoundList.push(sound)
        }
    };
    soundList = tempSoundList;
    console.log(' la soundlist est ');
    console.log(soundList);


})